| Method Sig                              | Responsibility                                            | Instance Vars Used      | Other Class Methods Called | Objects Used With Method Calls | Lines of Code |
| --------------------------------------- | --------------------------------------------------------- | ----------------------- | -------------------------- | ------------------------------ | ------------- |
| `public Player()`                       | Default constructor                                       | `name`                  | N/A                        | N/A                            | 3             |
| `public Player(String)`                 | Parameterized constructor                                 | N/A                     | `setName`                  | N/A                            | 3             |
| `public String getName()`               | Returns the player's name                                 | `name`                  | N/A                        | N/A                            | 3             |
| `protected void setName(String)`        | Sets the player's name                                    | `name`                  | N/A                        | N/A                            | 3             |
| `public Point getXyLocation()`          | Returns the player's loaction                             | `xyLocation`            | N/A                        | N/A                            | 3             |
| `public void setXyLocation(Point)`      | Sets the player's location                                | `xyLocation`            | N/A                        | N/A                            | 3             |
| `public Room getCurrentRoom()`          | Returns the room the player is in                         | `currentRoom`           | N/A                        | N/A                            | 3             |
| `protected void setCurrentRoom(Room)`   | Sets the room the player in in                            | `currentRoom`           | N/A                        | N/A                            | 3             |
| `public void addItem(Item)`             | Adds an item to the player's inventory                    | `inventory`             | N/A                        | ArrayList                      | 6             |
| `public ArrayList<Item> getInventory()` | Returns the player's inventory                            | `inventory`             | N/A                        | N/A                            | 3             |
| `public void equip(Item)`               | Sets an item as eqipped                                   | `eqipped`               | N/A                        | N/A                            | 3             |
| `public String displayInventory()`      | Returns a string representation of the player's inventory | `inventory`, `equipped` | N/A                        | Item                           | 17            |

