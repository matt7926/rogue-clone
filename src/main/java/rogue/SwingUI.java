package rogue;

import org.json.simple.parser.ParseException;

import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.swing.SwingTerminal;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.screen.Screen.RefreshType;

import javax.swing.JFrame;
import javax.swing.WindowConstants;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.border.EmptyBorder;

import java.awt.Container;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionEvent;

import java.io.IOException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;

public class SwingUI extends JFrame {
    private SwingTerminal terminal;
    private TerminalScreen screen;
    public static final int WIDTH = 850;
    public static final int HEIGHT = 450;
    public static final int TEXT_AREA_SIZE = 25;
    public static final int FONT_SIZE_SMALL = 12;
    public static final int FONT_SIZE_LARGE = 14;
    public static final int B_SIZE = 7;
    public static final int MESSAGE_SIZE = 87;
    public static final int NAME_SIZE = 15;
    private final char startCol = 0;
    private final char msgRow = 1;
    private final char roomRow = 3;
    private Container contentPane;
    private JTextArea inventoryArea;
    private JTextField messageField;
    private JTextField nameField;
    private static Rogue theGame;

    /**
    * Default constructor.
    */
    public SwingUI() {
        super("my awesome game");
        contentPane = getContentPane();
        setWindowDefaults(getContentPane());
        setUpPanels();
        pack();
        start();
        setVisible(true);
    }

    private void setWindowDefaults(Container theContentPane) {
        setTitle("Rogue!");
        setSize(WIDTH, HEIGHT);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        theContentPane.setLayout(new BorderLayout());
    }

    private void setTerminal() {
        JPanel terminalPanel = new JPanel();
        terminal = new SwingTerminal();
        terminalPanel.add(terminal);
        contentPane.add(terminalPanel, BorderLayout.CENTER);
    }

    private void setInventory() {

        inventoryArea = new JTextArea(TEXT_AREA_SIZE, TEXT_AREA_SIZE);
        inventoryArea.setEditable(false);
        inventoryArea.setBackground(Color.BLACK);
        inventoryArea.setForeground(Color.LIGHT_GRAY);
        inventoryArea.setFont(new Font("Courier", Font.PLAIN, FONT_SIZE_SMALL));
        JScrollPane inventoryPanel = new JScrollPane(inventoryArea);
        inventoryPanel.setBorder(new EmptyBorder(B_SIZE, B_SIZE, B_SIZE, B_SIZE));
        contentPane.add(inventoryPanel, BorderLayout.EAST);
    }

    private void setMenu() {
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        JMenuItem saveBtn = createSaveBtn();
        JMenuItem loadBtn = createLoadBtn();
        JMenuItem newGameBtn = createNewGameBtn();
        JMenuItem changeNameBtn = createChangeNameBtn();
        fileMenu.add(newGameBtn);
        fileMenu.add(loadBtn);
        fileMenu.add(saveBtn);
        fileMenu.add(changeNameBtn);
        menuBar.add(fileMenu);
        this.setJMenuBar(menuBar);
    }

    private JMenuItem createSaveBtn() {
        JMenuItem saveBtn = new JMenuItem("Save Game");
        saveBtn.addActionListener(
            (ActionEvent ev) -> {
                selectSaveLocation();
            }
        );
        return saveBtn;
    }

    private JMenuItem createLoadBtn() {
        JMenuItem loadBtn = new JMenuItem("Load Game");
        loadBtn.addActionListener(
            (ActionEvent ev) -> {
                selectFile();
            }
        );
        return loadBtn;
    }

    private JMenuItem createNewGameBtn() {
        JMenuItem newGameBtn = new JMenuItem("Load Map");
        newGameBtn.addActionListener(
            (ActionEvent ev) -> {
                selectRooms();
            }
        );
        return newGameBtn;
    }

    private JMenuItem createChangeNameBtn() {
        JMenuItem changeNameBtn = new JMenuItem("Set Name");
        changeNameBtn.addActionListener(
            (ActionEvent ev) -> {
                pickPlayerName();
            }
        );
        return changeNameBtn;
    }

    private void pickPlayerName() {
        String input = JOptionPane.showInputDialog(
            this,
            "Enter a new name",
            null,
            JOptionPane.QUESTION_MESSAGE
        );
        setPlayerName(input);
    }

    private void setMessageField() {
        messageField = new JTextField(MESSAGE_SIZE);
        messageField.setEditable(false);
        messageField.setBackground(Color.BLACK);
        messageField.setForeground(Color.LIGHT_GRAY);
        messageField.setFont(new Font("Courier", Font.PLAIN, FONT_SIZE_LARGE));
        nameField = new JTextField(NAME_SIZE);
        nameField.setBackground(Color.BLACK);
        nameField.setForeground(Color.LIGHT_GRAY);
        nameField.setCaretColor(Color.LIGHT_GRAY);
        nameField.setFont(new Font("Courier", Font.PLAIN, FONT_SIZE_LARGE));
        nameField.setEditable(false);
        JPanel messagePanel = new JPanel(new FlowLayout());
        messagePanel.add(messageField);
        messagePanel.add(nameField);
        contentPane.add(messagePanel, BorderLayout.NORTH);
    }

    protected void setPlayerName(String name) {
        theGame.getPlayer().setName(name);
        nameField.setText(theGame.getPlayer().getName());
        setMessage("Set name to " + theGame.getPlayer().getName());
    }

    private void selectSaveLocation() {
        JFileChooser fc = new JFileChooser();
        int retVal = fc.showSaveDialog(this);
        if (retVal == JFileChooser.APPROVE_OPTION) {
            String fileName = fc.getSelectedFile().getAbsolutePath();
            save(theGame, fileName);
        }
    }

    private void selectFile() {
        JFileChooser fc = new JFileChooser();
        int retVal = fc.showOpenDialog(this);
        if (retVal == JFileChooser.APPROVE_OPTION) {
            String fileName = fc.getSelectedFile().getAbsolutePath();
            loadGame(theGame, fileName);
        }
    }

    private void selectRooms() {
        JFileChooser fc = new JFileChooser();
        int retVal = fc.showOpenDialog(this);
        if (retVal == JFileChooser.APPROVE_OPTION) {
            String fileName = fc.getSelectedFile().getAbsolutePath();
            try {
                RogueParser newRp = new RogueParser(fileName);
                Rogue newGame = new Rogue(newRp);
                theGame.setItems(newGame.getItems());
                theGame.setRooms(newGame.getRooms());
                theGame.setSymbols(newGame.getSymbols());
                theGame.setNextDisplay(newGame.getNextDisplay());
                theGame.setPlayer(new Player("Player 1"));
                this.draw(" ", theGame.getNextDisplay());
                this.drawInventory(theGame.getPlayer().displayInventory());
            } catch (IOException | ParseException e) {
                loadAlert();
            }
        }
    }

    private void setUpPanels() {
        setTerminal();
        setInventory();
        setMenu();
        setMessageField();
    }

    private void start() {
        try {
            screen = new TerminalScreen(terminal);
            screen.setCursorPosition(TerminalPosition.TOP_LEFT_CORNER);
            screen.startScreen();
            screen.refresh();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void putString(String toDisplay, int column, int row) {
        Terminal t = screen.getTerminal();
        try {
            t.setCursorPosition(column, row);
            for (char ch: toDisplay.toCharArray()) {
                t.putCharacter(ch);
                if (ch == '\n') {
                    t.setCursorPosition(column, ++row);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void setMessage(String msg) {
        messageField.setText(msg);
    }

    protected void draw(String message, String room) {
        try {
            screen.refresh(RefreshType.COMPLETE);
            setMessage(message);
            putString(room, startCol, roomRow);
            screen.refresh();
            screen.getTerminal().setCursorPosition(0, 0);
        } catch (IOException e) {
            //
        }
    }

    protected char getInput() {
        KeyStroke keyStroke = null;
        char returnChar;
        while (keyStroke == null) {
            try {
                keyStroke = screen.pollInput();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        returnChar = parseInput(keyStroke);
        return returnChar;
    }

    private char parseInput(KeyStroke ks) {
        if (ks.getKeyType() == KeyType.ArrowDown) {
            return Rogue.DOWN;  //constant defined in rogue
        } else if (ks.getKeyType() == KeyType.ArrowUp) {
            return Rogue.UP;
        } else if (ks.getKeyType() == KeyType.ArrowLeft) {
            return Rogue.LEFT;
        } else if (ks.getKeyType() == KeyType.ArrowRight) {
            return Rogue.RIGHT;
        } else if (ks.getCharacter() != null) {
            return ks.getCharacter();
        }
        return ' ';
    }

    /**
    * Saves a serialized copy of the current game.
    * @param game the current game object
    * @param filename the name of the file to be saved
    */
    public void save(Rogue game, String filename) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename));
            out.writeObject(game);
            out.close();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    /**
    * Loads a serialized copy of the current game.
    * @param game the game to be overwritten
    * @param filename the file to be loaded
    */
    public void loadGame(Rogue game, String filename) {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename));
            Rogue loadedGame = (Rogue) in.readObject();
            game.setItems(loadedGame.getItems());
            game.setRooms(loadedGame.getRooms());
            game.setSymbols(loadedGame.getSymbols());
            game.setNextDisplay(loadedGame.getNextDisplay());
            game.setPlayer(loadedGame.getPlayer());
            this.draw(" ", game.getNextDisplay());
            this.drawInventory(game.getPlayer().displayInventory());
            this.setPlayerName(game.getPlayer().getName());
        } catch (IOException ex) {
            loadAlert();
        } catch (ClassNotFoundException ex) {
            loadAlert();
        }
    }

    private void loadAlert() {
        JOptionPane.showMessageDialog(
            this,
            "Could not load file",
            "Warning",
            JOptionPane.WARNING_MESSAGE
        );
    }

    protected void drawInventory(String inv) {
        inventoryArea.setText(inv);
    }

    /**
    * The main function of the program.
    * @param args command line arguments
    */
    public static void main(String[] args) {
        char userInput = '.';
        String message;
        String configurationFileLocation = "fileLocations.json";
        SwingUI theGameUI = new SwingUI();
        RogueParser parser = null;
        try {
            parser = new RogueParser(configurationFileLocation);
        } catch (IOException | ParseException e) {
            theGameUI.loadAlert();
        }
        theGame = new Rogue(parser);
        Player thePlayer = new Player("Player 1");
        theGame.setPlayer(thePlayer);
        message = "Welcome to my Rogue game";
        theGameUI.setPlayerName(thePlayer.getName());
        theGameUI.draw(message, theGame.getNextDisplay());
        theGameUI.drawInventory(thePlayer.displayInventory());
        theGameUI.gameLoop(userInput, theGameUI, message, theGame);
    }

    protected void gameLoop(char userInput, SwingUI theGameUI, String message, Rogue game) {
        while (userInput != 'q') {
            userInput = theGameUI.getInput();
            if (userInput == 'e' || userInput == 't' || userInput == 'c') {
                message = specialInput(userInput);
            } else {
                try {
                    message = game.makeMove(userInput);
                } catch (InvalidMoveException badMove) {
                    message = badMove.getMessage();
                }
            }
            Player thePlayer = game.getPlayer();
            theGameUI.draw(message, game.getNextDisplay());
            theGameUI.drawInventory(thePlayer.displayInventory());
        }
        System.exit(0);
    }

    private String specialInput(char userInput) {
        String msg = "";
        if (userInput == 'e') {
            msg = promptEat();
        } else if (userInput == 't') {
            msg = promptToss();
        } else if (userInput == 'c') {
            msg = promptWear();
        }
        return msg;
    }

    private String promptEat() {
        String msg = " ";
        try {
            int index = promptUser("eat");
            msg = theGame.eatItem(index);
        } catch (NumberFormatException e) {
            return "Not a valid number";
        }
        return msg;
    }

    private String promptToss() {
        String msg = " ";
        try {
            int index = promptUser("toss");
            msg = theGame.tossItem(index);
        } catch (NumberFormatException e) {
            return "Not a valid number";
        }
        return msg;
    }

    private String promptWear() {
        String msg = " ";
        try {
            int index = promptUser("wear");
            msg = theGame.wearItem(index);
        } catch (NumberFormatException e) {
            return "Not a valid number";
        }
        return msg;
    }

    private int promptUser(String type) throws NumberFormatException {
        String input = JOptionPane.showInputDialog(
            this,
            "Select an item to " + type,
            null,
            JOptionPane.QUESTION_MESSAGE
        );
        try {
            return Integer.valueOf(input);
        } catch (NumberFormatException e) {
            throw new NumberFormatException();
        }
    }
}
