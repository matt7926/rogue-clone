package rogue;

import java.util.ArrayList;

import java.awt.Point;

import java.io.Serializable;

/**
* Player contains information about the player character.
*/
public class Player implements Serializable {
    private static final long serialVersionUID = -4313455218710817037L;
    private String name;
    private Room currentRoom;
    private Point xyLocation = new Point(1, 1);
    private Item equipped = null;
    private ArrayList<Item> inventory = new ArrayList<Item>();

    /**
    * Default constructor, sets name to null.
    */
    public Player() {
        this.name = null;
    }

    /**
    * Constructor with parameters.
    * @param theName the name of the player
    */
    public Player(String theName) {
        setName(theName);
    }

    /**
    * Returns the player's name.
    * @return the player's name
    */
    public String getName() {
        return name;
    }


    protected void setName(String newName) {
        name = newName;
    }

    /**
    * Returns the player's position.
    * @return a point object of the player's position
    */
    public Point getXyLocation() {
        return xyLocation;
    }

    /**
    * Sets the player's position.
    * @param newXyLocation the player's new position
    */
    public void setXyLocation(Point newXyLocation) {
        xyLocation = newXyLocation;
    }

    /**
    * Returns the current room.
    * @return the current room
    */
    public Room getCurrentRoom() {
        return currentRoom;
    }

    protected void setCurrentRoom(Room newRoom) {
        currentRoom = newRoom;
    }

    /**
    * Adds an item to the player's inventory.
    * @param toAdd the item to be added
    */
    public void addItem(Item toAdd) {
        if (inventory == null) {
            inventory = new ArrayList<Item>();
        }
        inventory.add(toAdd);
    }

    /**
    * Returns the player's inventory.
    * @return the player's inventory
    */
    public ArrayList<Item> getInventory() {
        return inventory;
    }

    /**
    * Sets the player's equipped item.
    * @param toEquip the item to equip
    */
    public void equip(Item toEquip) {
        equipped = toEquip;
    }

    /**
    * Returns a string representation of the player's inventory.
    * @return a string representation of the player's inventory
    */
    public String displayInventory() {
        String inv = "Inventory\n---------\n";
        if (inventory != null) {
            int c = 0;
            for (Item i : inventory) {
                inv += c + ". " + i.getName();
                if (equipped != null) {
                    if (i.getId() == equipped.getId()) {
                        inv += " (E)";
                    }
                }
                inv += "\n";
                c++;
            }
        }
        return inv;
    }
}
