package rogue;
public class ImpossiblePositionException extends Exception {
    /**
    * Default constructor.
    */
    public ImpossiblePositionException() {
        super();
    }
    /**
    * Constructor with single parameter.
    * @param message the error message
    */
    public ImpossiblePositionException(String message) {
        super(message);
    }
}
