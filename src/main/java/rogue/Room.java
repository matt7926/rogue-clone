package rogue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Arrays;

import java.awt.Point;

import java.io.Serializable;


/**
 * A room within the dungeon - contains monsters, treasure,
 * doors out, etc.
 */
public class Room implements Serializable {
    private static final long serialVersionUID = -3367273921420429127L;
    private int width;
    private int height;
    private int id;
    private Player thePlayer;
    private Boolean playerInRoom = false;
    private HashMap<Character, Door> doors = null;
    private ArrayList<Item> roomItems = null;
    private HashMap<String, Character> roomSymbols;
    private Rogue currentGame;

    private static final int DEFAULT_WIDTH = 20;
    private static final int DEFAULT_HEIGHT = 20;

    /**
    * Default constructor.
    */
    public Room() {
        setWidth(DEFAULT_WIDTH);
        setHeight(DEFAULT_HEIGHT);
        doors = new HashMap<Character, Door>();
        roomItems = new ArrayList<Item>();
    }

    /**
    * Constructor with parameters.
    * @param theWidth the room's width
    * @param theHeight the room's height
    * @param theId the room's id
    * @param theRoomSymbols the symbol map for the room
    * @param theNewPlayer the player character
    */
    public Room(int theWidth, int theHeight, int theId, HashMap<String, Character> theRoomSymbols,
        Player theNewPlayer) {
        setWidth(theWidth);
        setHeight(theHeight);
        setId(theId);
        setRoomSymbols(theRoomSymbols);
        setPlayer(theNewPlayer);
    }

    /**
    * Constructor with parameters.
    * @param theWidth the room's width
    * @param theHeight the room's height
    * @param theId the room's id
    * @param theRoomSymbols the symbol map for the room
    * @param theNewPlayer the player character
    * @param theDoors the doors in the room
    */
    public Room(int theWidth, int theHeight, int theId, HashMap<String, Character> theRoomSymbols,
        Player theNewPlayer, ArrayList<Door> theDoors) {
        setWidth(theWidth);
        setHeight(theHeight);
        setId(theId);
        setRoomSymbols(theRoomSymbols);
        setPlayer(theNewPlayer);
        setAllDoors(theDoors);
    }

    /**
    * Return the Rogue game assosciated with this room.
    * @return the assosciated rogue game
    */
    public Rogue getGame() {
        return currentGame;
    }

    protected void setGame(Rogue theGame) {
        currentGame = theGame;
    }

    /**
    * Returns the room's width.
    * @return the room's width
    */
    public int getWidth() {
        return width;
    }

    protected void setWidth(int newWidth) {
        width = newWidth;
    }

    /**
    * Returns the room's height.
    * @return the room's height
    */
    public int getHeight() {
        return height;
    }

    protected void setHeight(int newHeight) {
        height = newHeight;
    }

    /**
    * Returns the room's id.
    * @return the room's id
    */
    public int getId() {
        return id;
    }

    protected void setId(int newId) {
        id = newId;
    }

    /**
    * Returns the items in the room.
    * @return the items in the room
    */
    public ArrayList<Item> getRoomItems() {
        return roomItems;
    }

    protected void setRoomItems(ArrayList<Item> newRoomItems) {
        roomItems = newRoomItems;
    }

    /**
    * Returns the player.
    * @return the player
    */
    public Player getPlayer() {
        return thePlayer;
    }

    protected void setPlayer(Player newPlayer) {
        thePlayer = newPlayer;
    }

    /**
    * Returns a door in a given direction.
    * @param direction the direction to get the door from
    * @return the door in the given direction
    */
    public Door getDoor(String direction) {
        if (doors.containsKey(direction.charAt(0))) {
            return doors.get(direction.charAt(0));
        }
        return null;
    }

    protected void setAllDoors(ArrayList<Door> theDoors) {
        if (doors == null) {
            doors = new HashMap<>();
        }
        if (theDoors != null) {
            for (Door d : theDoors) {
                doors.put(d.getDirection(), d);
            }
        }
    }

    protected void setRoomSymbols(HashMap<String, Character> theRoomSymbols) {
        roomSymbols = theRoomSymbols;
    }

    protected void setPlayerInRoom(Boolean isPlayerInRoom) {
        playerInRoom = isPlayerInRoom;
    }

    /**
    * Returns if the player is in the room.
    * @return if the player is in the room
    */
    public boolean isPlayerInRoom() {
        return playerInRoom;
    }

    /**
    * Adds an item to the room.
    * @param theItem the item to be added to the room
    * @throws ImpossiblePositionException if the item cannot be placed
    * @throws NoSuchItemException if the item does not exist
    */
    public void addItem(Item theItem) throws ImpossiblePositionException, NoSuchItemException {
        if (roomItems == null) {
            roomItems = new ArrayList<Item>();
        }
        Point itemPos = theItem.getXyLocation();
        Point playerPos = thePlayer.getXyLocation();
        if (itemDoesNotExist(theItem)) {
            throw new NoSuchItemException();
        }
        if (itemInWall(itemPos) || itemOnItem(itemPos) || itemOnPlayer(itemPos)) {
            throw new ImpossiblePositionException();
        }
        roomItems.add(theItem);
    }

    protected boolean itemDoesNotExist(Item theItem) {
        for (Item i : currentGame.getItems()) {
            if (theItem.getId() == i.getId()) {
                return false;
            }
        }
        return true;
    }

    protected boolean itemInWall(Point itemPos) {
        if (itemPos.x <= 0 || itemPos.y <= 0 || itemPos.x >= width - 1 || itemPos.y >= height - 1) {
            return true;
        }
        return false;
    }

    protected boolean itemOnItem(Point itemPos) {
        for (Item i : roomItems) {
            Point currentItemPos = i.getXyLocation();
            if (currentItemPos.x == itemPos.x && currentItemPos.y == itemPos.y) {
                return true;
            }
        }
        return false;
    }

    protected boolean itemOnPlayer(Point itemPos) {
        Point playerPos = thePlayer.getXyLocation();
        if (playerInRoom && itemPos.x == playerPos.x && itemPos.y == playerPos.y) {
            return true;
        }
        return false;
    }

    protected void addDoor(Door theDoor) {
        if (doors == null) {
            doors = new HashMap<>();
        }
        doors.put(theDoor.getDirection(), theDoor);
    }

    /**
    * Verifies if a room has all the required elements.
    * @return Returns whether or not the room is complete
    * @throws NotEnoughDoorsException if the room has no doors
    */
    public boolean verifyRoom() throws NotEnoughDoorsException {
        boolean isValidRoom = false;
        if (hasDoors()) {
            isValidRoom = true;
        } else {
            throw new NotEnoughDoorsException();
        }
        if (hasValidItemPositions() && hasPlayerInValidPosition()) {
            isValidRoom = true;
        } else {
            isValidRoom = false;
        }
        return isValidRoom;
    }

    protected boolean hasDoors() {
        String dirs = "NESW";
        for (char c : dirs.toCharArray()) {
            if (doors.containsKey(c)) {
                return true;
            }
        }
        return false;
    }

    protected boolean hasValidItemPositions() {
        if (roomItems == null) {
            return true;
        }
        for (Item i : roomItems) {
            Point iPos = i.getXyLocation();
            if (iPos.x <= 0 || iPos.x >= width - 1 || iPos.y <= 0 || iPos.y >= height - 1) {
                return false;
            }
            for (Item j : roomItems) {
                Point jPos = j.getXyLocation();
                if (iPos.x == jPos.x && iPos.y == jPos.y && i.getId() != j.getId()) {
                    return false;
                }
            }
        }
        return true;
    }

    protected boolean hasPlayerInValidPosition() {
        Point pPos = thePlayer.getXyLocation();
        if (pPos.x <= 0 || pPos.x >= width - 1 || pPos.y <= 0 || pPos.y >= height - 1) {
            return false;
        }
        return true;
    }

    /**
    * Returns a string representation of the room.
    * @return a string representation of the room
    */
    public String displayRoom() {
        char[][] roomArray = new char[height][width];
        String roomString = "";
        setFloorsWalls(roomArray);
        setItems(roomArray);
        setPlayerChar(roomArray);
        setDoors(roomArray);
        for (int i = 0; i < height; i++) {
            roomString += String.valueOf(roomArray[i]) + "\n";
        }
        return roomString;
    }

    protected void setFloorsWalls(char[][] roomArray) {
        for (char[] row : roomArray) {
            Arrays.fill(row, roomSymbols.get("FLOOR"));
            row[0] = roomSymbols.get("EW_WALL");
            row[width - 1] = roomSymbols.get("EW_WALL");
        }
        Arrays.fill(roomArray[0], roomSymbols.get("NS_WALL"));
        Arrays.fill(roomArray[height - 1], roomSymbols.get("NS_WALL"));
    }

    protected void setItems(char[][] roomArray) {
        Point p;
        if (roomItems != null) {
            for (Item i : roomItems) {
                p = i.getXyLocation();
                roomArray[p.y][p.x] = i.getDisplayCharacter();
            }
        }
    }

    protected void setPlayerChar(char[][] roomArray) {
        if (playerInRoom) {
            roomArray[thePlayer.getXyLocation().y][thePlayer.getXyLocation().x]
                = roomSymbols.get("PLAYER");
        }
    }

    protected void setDoors(char[][] roomArray) {
        Door d;
        if (doors.containsKey('N')) {
            d = doors.get('N');
            roomArray[0][d.getWallPosition()] = roomSymbols.get("DOOR");
        }
        if (doors.containsKey('E')) {
            d = doors.get('E');
            roomArray[d.getWallPosition()][width - 1] = roomSymbols.get("DOOR");
        }
        if (doors.containsKey('S')) {
            d = doors.get('S');
            roomArray[height - 1][d.getWallPosition()] = roomSymbols.get("DOOR");
        }
        if (doors.containsKey('W')) {
            d = doors.get('W');
            roomArray[d.getWallPosition()][0] = roomSymbols.get("DOOR");
        }
    }
}
