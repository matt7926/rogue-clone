package rogue;

import java.util.ArrayList;

import java.io.Serializable;

public class Door implements Serializable {
    private static final long serialVersionUID = -3736839410578024258L;
    private char direction;
    private ArrayList<Room> connectedRooms = new ArrayList<Room>();
    private int wallPosition;
    /**
    * Default constructor.
    */
    public Door() {
        wallPosition = 1;
        connectedRooms = new ArrayList<Room>();
    }

    /**
    * Constructor with parameters.
    * @param theDirection the direction the wall the door is on is facing
    * @param theWallPosition the position of the door on the wall
    */
    public Door(char theDirection, int theWallPosition) {
        setDirection(theDirection);
        setWallPos(theWallPosition);
    }

    protected void setDirection(char theDirection) {
        direction = theDirection;
    }

    protected void setConnedctedRooms(ArrayList<Room> theConnectedRooms) {
        connectedRooms = theConnectedRooms;
    }

    protected void setWallPos(int theWallPosition) {
        wallPosition = theWallPosition;
    }

    /**
    * Returns the direction of the door.
    * @return the direction of the door
    */
    public char getDirection() {
        return direction;
    }

    /**
    * Returns the wall position of the door.
    * @return the wall position of the door
    */
    public int getWallPosition() {
        return wallPosition;
    }

    /**
    * Connects the door to a second room.
    * @param newRoom the room to be connected
    */
    public void connectRoom(Room newRoom) {
        connectedRooms.add(newRoom);
    }

    /**
    * Returns the other room a door is connected to given 1 door.
    * @param currentRoom a room the door connects to
    * @return the other room connected to the door
    */
    public Room getOtherRoom(Room currentRoom) {
        if (currentRoom.getId() == connectedRooms.get(0).getId()) {
            return connectedRooms.get(1);
        } else {
            return connectedRooms.get(0);
        }
    }
}
