package rogue;
public class NoSuchItemException extends Exception {
    /**
    * Default constructor.
    */
    public NoSuchItemException() {
        super();
    }

    /**
    * Constructor with single parameter.
    * @param message the error message
    */
    public NoSuchItemException(String message) {
        super(message);
    }
}
