package rogue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import java.io.Serializable;

import java.awt.Point;

public class Rogue implements Serializable {
    private static final long serialVersionUID = 97098967324988707L;
    public static final char UP = 'w';
    public static final char DOWN = 's';
    public static final char LEFT = 'a';
    public static final char RIGHT = 'd';
    private ArrayList<Item> items = new ArrayList<Item>();
    private ArrayList<Room> rooms = new ArrayList<Room>();
    private HashMap<String, Character> roomSymbols = new HashMap<String, Character>();
    private Player thePlayer = new Player();
    private String nextDisplay;

    /**
    * Default constructor.
    */
    public Rogue() {
        items = new ArrayList<Item>();
        rooms = new ArrayList<Room>();
        roomSymbols = new HashMap<String, Character>();
        thePlayer = new Player();
    }

    /**
    * Constructor with parameter.
    * @param theDungeonInfo a RogueParser object with information about the dungeon
    */
    public Rogue(RogueParser theDungeonInfo) {
        setSymbols(theDungeonInfo);
        theDungeonInfo.reset();
        createRooms(theDungeonInfo);
        nextDisplay = thePlayer.getCurrentRoom().displayRoom();
    }

    /**
    * Set the player variable.
    * @param newPlayer the Player to set as the character
    */
    public void setPlayer(Player newPlayer) {
        thePlayer = newPlayer;
        for (Room r : rooms) {
            if (r.isPlayerInRoom()) {
                thePlayer.setCurrentRoom(r);
                break;
            }
        }
    }

    /**
    * Returns the player object.
    * @return the player object
    */
    public Player getPlayer() {
        return thePlayer;
    }

    /**
    * Set the character representaions of different objects.
    * @param rp a RogueParser with information about the dungeon
    */
    public void setSymbols(RogueParser rp) {
        roomSymbols.put("PASSAGE", rp.getSymbol("PASSAGE"));
        roomSymbols.put("DOOR", rp.getSymbol("DOOR"));
        roomSymbols.put("FLOOR", rp.getSymbol("FLOOR"));
        roomSymbols.put("PLAYER", rp.getSymbol("PLAYER"));
        roomSymbols.put("GOLD", rp.getSymbol("GOLD"));
        roomSymbols.put("NS_WALL", rp.getSymbol("NS_WALL"));
        roomSymbols.put("EW_WALL", rp.getSymbol("EW_WALL"));
        roomSymbols.put("POTION", rp.getSymbol("POTION"));
        roomSymbols.put("SCROLL", rp.getSymbol("SCROLL"));
        roomSymbols.put("ARMOR", rp.getSymbol("ARMOR"));
        roomSymbols.put("FOOD", rp.getSymbol("FOOD"));
    }

    /**
    * Set the character representaions of different objects.
    * @param symbols a HashMap of the symbols
    */
    public void setSymbols(HashMap<String, Character> symbols) {
        roomSymbols = symbols;
    }

    /**
    * Returns the symbol HashMap.
    * @return the symbol HashMap
    */
    public HashMap<String, Character> getSymbols() {
        return roomSymbols;
    }

    /**
    * Create the required room objects from the rooms JSON file.
    * @param rp a RogueParser with information about the dungeon
    */
    public void createRooms(RogueParser rp) {
        addAllRooms(rp);
        rp.reset();
        addAllItems(rp);
        rp.reset();
        addAllDoors(rp);
    }

    protected void addAllRooms(RogueParser rp) {
        Map roomMap = rp.nextRoom();
        while (roomMap != null) {
            addRoom(roomMap);
            roomMap = rp.nextRoom();
        }
    }

    /**
    * Adds a room to the ArrayList of rooms.
    * @param roomMap a map of the room's properties
    */
    public void addRoom(Map<String, String> roomMap) {
        Room r = new Room(Integer.parseInt(roomMap.get("width").toString()),
            Integer.parseInt(roomMap.get("height").toString()),
            Integer.parseInt(roomMap.get("id").toString()), roomSymbols, thePlayer);

        r.setRoomItems(null);
        r.setAllDoors(null);
        Boolean inRoom = Boolean.parseBoolean((String) roomMap.get("start"));
        r.setPlayerInRoom(inRoom);
        if (inRoom) {
            thePlayer.setCurrentRoom(r);
            r.setPlayer(thePlayer);
        }
        r.setGame(this);
        rooms.add(r);
    }

    protected void addAllItems(RogueParser rp) {
        Map itemMap = rp.nextItem();
        while (itemMap != null) {
            addItem(itemMap);
            itemMap = rp.nextItem();
        }
    }

    /**
    * Adds an item to the ArrayList of items and to its room.
    * @param itemMap A map of the item's properties
    */
    public void addItem(Map<String, String> itemMap) {
        Item toAdd = createItem(itemMap);
        if (itemMap.containsKey("room")) {
            addToRoom(toAdd, itemMap);
        } else {
            items.add(toAdd);
        }
    }

    protected Item createItem(Map<String, String> itemMap) {
        String type = itemMap.get("type").toString();
        if (isSpecialItem(type)) {
            return (createSpecialItem(type, itemMap));
        }
        int id = Integer.parseInt(itemMap.get("id").toString());
        String name = itemMap.get("name").toString();
        String description = itemMap.get("description").toString();
        Item toAdd = new Item(id, name, type, description);
        toAdd.setDisplayCharacter(roomSymbols.get(type.toUpperCase()));
        return toAdd;
    }

    protected boolean isSpecialItem(String type) {
        if (type.equals("Food")) {
            return true;
        } else if (type.equals("SmallFood")) {
            return true;
        } else if (type.equals("Clothing")) {
            return true;
        } else if (type.equals("Magic")) {
            return true;
        } else if (type.equals("Potion")) {
            return true;
        } else if (type.equals("Ring")) {
            return true;
        }
        return false;
    }

    protected Item createSpecialItem(String type, Map<String, String> itemMap) {
        if (type.equals("Food")) {
            return createFood(itemMap);
        } else if (type.equals("SmallFood")) {
            return createSmallFood(itemMap);
        } else if (type.equals("Clothing")) {
            return createClothing(itemMap);
        } else if (type.equals("Magic")) {
            return createMagic(itemMap);
        } else if (type.equals("Potion")) {
            return createPotion(itemMap);
        } else if (type.equals("Ring")) {
            return createRing(itemMap);
        }
        return null;
    }

    protected Food createFood(Map<String, String> itemMap) {
        int id = Integer.parseInt(itemMap.get("id").toString());
        String name = itemMap.get("name").toString();
        String description = itemMap.get("description").toString();
        Food toAdd = new Food(id, name, "Food", description);
        toAdd.setDisplayCharacter(':');
        return toAdd;
    }

    protected SmallFood createSmallFood(Map<String, String> itemMap) {
        int id = Integer.parseInt(itemMap.get("id").toString());
        String name = itemMap.get("name").toString();
        String description = itemMap.get("description").toString();
        SmallFood toAdd = new SmallFood(id, name, "SmallFood", description);
        toAdd.setDisplayCharacter(':');
        return toAdd;
    }

    protected Clothing createClothing(Map<String, String> itemMap) {
        int id = Integer.parseInt(itemMap.get("id").toString());
        String name = itemMap.get("name").toString();
        String description = itemMap.get("description").toString();
        Clothing toAdd = new Clothing(id, name, "Clothing", description);
        toAdd.setDisplayCharacter(']');
        return toAdd;
    }

    protected Magic createMagic(Map<String, String> itemMap) {
        int id = Integer.parseInt(itemMap.get("id").toString());
        String name = itemMap.get("name").toString();
        String description = itemMap.get("description").toString();
        Magic toAdd = new Magic(id, name, "Magic", description);
        toAdd.setDisplayCharacter('?');
        return toAdd;
    }

    protected Potion createPotion(Map<String, String> itemMap) {
        int id = Integer.parseInt(itemMap.get("id").toString());
        String name = itemMap.get("name").toString();
        String description = itemMap.get("description").toString();
        Potion toAdd = new Potion(id, name, "Potion", description);
        toAdd.setDisplayCharacter('!');
        return toAdd;
    }

    protected Ring createRing(Map<String, String> itemMap) {
        int id = Integer.parseInt(itemMap.get("id").toString());
        String name = itemMap.get("name").toString();
        String description = itemMap.get("description").toString();
        Ring toAdd = new Ring(id, name, "Ring", description);
        toAdd.setDisplayCharacter('o');
        return toAdd;
    }

    protected Item addToRoom(Item toAdd, Map<String, String> itemMap) {
        int roomId = Integer.parseInt(itemMap.get("room").toString());
        int x = Integer.parseInt(itemMap.get("x").toString());
        int y = Integer.parseInt(itemMap.get("y").toString());
        toAdd.setXyLocation(new Point(x, y));
        for (Room r : rooms) {
            if (r.getId() == roomId) {
                toAdd.setCurrentRoom(r);
                items.add(toAdd);
                try {
                    r.addItem(toAdd);
                } catch (ImpossiblePositionException ie) {
                    toAdd = findNewPosition(toAdd, r);
                } catch (NoSuchItemException ne) {
                    break;
                }
            }
        }
        return toAdd;
    }

    protected Item findNewPosition(Item toAdd, Room r) {
        tryloop:
        for (int i = 1; i < r.getHeight() - 1; i++) {
            for (int j = 1; j < r.getWidth() - 1; j++) {
                toAdd.setXyLocation(new Point(j, i));
                try {
                    r.addItem(toAdd);
                    break tryloop;
                } catch (ImpossiblePositionException ie2) {
                    continue;
                } catch (NoSuchItemException ne) {
                    break;
                }
            }
        }
        return toAdd;
    }

    protected void addAllDoors(RogueParser rp) {
        Map rm = rp.nextRoom();
        while (rm != null) {
            parseDoor(rm);
            rm = rp.nextRoom();
        }
        for (Room r : rooms) {
            verifyRoom(r);
        }
    }

    protected void parseDoor(Map rm) {
        for (Character c : "NESW".toCharArray()) {
            int wallPos = Integer.parseInt(rm.get(c.toString()).toString());
            if (wallPos != -1) {
                int currRoom = Integer.parseInt(rm.get("id").toString());
                int conRoom = Integer.parseInt(rm.get("CON_" + c).toString());
                Door toAdd = new Door(c, wallPos);
                for (Room r : rooms) {
                    if (r.getId() == currRoom || r.getId() == conRoom) {
                        toAdd.connectRoom(r);
                    }
                }
                for (Room r : rooms) {
                    if (r.getId() == currRoom) {
                        r.addDoor(toAdd);
                    }
                }
            }
        }
    }

    protected void verifyRoom(Room r) {
        try {
            r.verifyRoom();
        } catch (NotEnoughDoorsException e) {
            loop:
            for (Room r2 : rooms) {
                if (r.getId() != r2.getId()) {
                    for (char c : "NESW".toCharArray()) {
                        if (r2.getDoor(String.valueOf(c)) == null) {
                            createConnection(r, r2, c);
                            break loop;
                        }
                    }
                }
            }
        }
    }

    protected void createConnection(Room toConnect, Room toBeConnected, char dir) {
        Door addToBeConnected = null;
        Door addToConnect = null;
        if (dir == 'N') {
            addToBeConnected = new Door('N', toBeConnected.getWidth() / 2);
            addToConnect = new Door('S', toConnect.getWidth() / 2);
        } else if (dir == 'E') {
            addToBeConnected = new Door('E', toBeConnected.getHeight() / 2);
            addToConnect = new Door('W', toConnect.getHeight() / 2);
        } else if (dir == 'S') {
            addToBeConnected = new Door('S', toBeConnected.getWidth() / 2);
            addToConnect = new Door('N', toConnect.getWidth() / 2);
        } else if (dir == 'W') {
            addToBeConnected = new Door('W', toBeConnected.getHeight() / 2);
            addToConnect = new Door('E', toConnect.getHeight() / 2);
        }
        connectRooms(toConnect, toBeConnected, addToBeConnected, addToConnect);
    }

    protected void connectRooms(Room toConnect, Room toBeConnected,
        Door addToBeConnected, Door addToConnect) {
            addToBeConnected.connectRoom(toConnect);
            addToBeConnected.connectRoom(toBeConnected);
            addToConnect.connectRoom(toConnect);
            addToConnect.connectRoom(toBeConnected);
            toBeConnected.addDoor(addToBeConnected);
            toConnect.addDoor(addToConnect);
    }

    /**
    * Set the rooms array.
    * @param newRooms the rooms array to be set
    */
    public void setRooms(ArrayList<Room> newRooms) {
        rooms = newRooms;
    }

    /**
    * Returns all rooms.
    * @return an ArrayList of all rooms
    */
    public ArrayList<Room> getRooms() {
        return rooms;
    }

    /**
    * Set the items array.
    * @param newItems the items array to be set
    */
    public void setItems(ArrayList<Item> newItems) {
        items = newItems;
    }

    /**
    * Returns all items.
    * @return an ArrayList of all items
    */
    public ArrayList<Item> getItems() {
        return items;
    }


    /**
    * Returns a string representation of all rooms.
    * @return a string representation of all rooms
    */
    public String displayAll() {
        String all = "";
        for (Room r : rooms) {
            all += r.displayRoom() + "\n\n";
        }
        //Remove extra \n
        all = all.substring(0, all.length());
        return all;
    }

    /**
    * Updates nextDisplay based of the input of the player.
    * @param input the key pressed by the player
    * @return a message for the player
    * @throws InvalidMoveException if the player tries to walk into a wall
    */
    public String makeMove(char input) throws InvalidMoveException {
        Room currentRoom = thePlayer.getCurrentRoom();
        Point currentPos = thePlayer.getXyLocation();
        String message = " ";
        if (input == 's') {
            message = moveDown();
        } else if (input == 'w') {
            message = moveUp();
        } else if (input == 'd') {
            message = moveRight();
        } else if (input == 'a') {
            message = moveLeft();
        }
        currentRoom = thePlayer.getCurrentRoom();
        currentRoom.setPlayer(thePlayer);
        nextDisplay = currentRoom.displayRoom();
        return message;
    }

    protected String moveDown() throws InvalidMoveException {
        Room currentRoom = thePlayer.getCurrentRoom();
        Point currentPos = thePlayer.getXyLocation();
        Point newPos = new Point(currentPos.x, currentPos.y);
        newPos.y++;
        if (isWall('s', newPos, currentRoom) && !isDoor('s', newPos, currentRoom)) {
            throw new InvalidMoveException("That's a wall");
        } else if (isDoor('s', newPos, currentRoom)) {
            Room conRoom = currentRoom.getDoor("S").getOtherRoom(currentRoom);
            Door conDoor = conRoom.getDoor("N");
            thePlayer.setXyLocation(new Point(conDoor.getWallPosition(), 1));
            thePlayer.setCurrentRoom(conRoom);
            currentRoom.setPlayerInRoom(false);
            conRoom.setPlayerInRoom(true);
            return ("Room " + conRoom.getId());
        }
        thePlayer.setXyLocation(newPos);
        return itemCheck(newPos, currentRoom);
    }

    protected String moveUp() throws InvalidMoveException {
        Room currentRoom = thePlayer.getCurrentRoom();
        Point currentPos = thePlayer.getXyLocation();
        Point newPos = new Point(currentPos.x, currentPos.y);
        newPos.y--;
        if (isWall('w', newPos, currentRoom) && !isDoor('w', newPos, currentRoom)) {
            throw new InvalidMoveException("That's a wall");
        } else if (isDoor('w', newPos, currentRoom)) {
            Room conRoom = currentRoom.getDoor("N").getOtherRoom(currentRoom);
            Door conDoor = conRoom.getDoor("S");
            thePlayer.setXyLocation(new Point(conDoor.getWallPosition(), conRoom.getHeight() - 2));
            thePlayer.setCurrentRoom(conRoom);
            currentRoom.setPlayerInRoom(false);
            conRoom.setPlayerInRoom(true);
            return ("Room " + conRoom.getId());
        }
        thePlayer.setXyLocation(newPos);
        return itemCheck(newPos, currentRoom);
    }

    protected String moveRight() throws InvalidMoveException {
        Room currentRoom = thePlayer.getCurrentRoom();
        Point currentPos = thePlayer.getXyLocation();
        Point newPos = new Point(currentPos.x, currentPos.y);
        newPos.x++;
        if (isWall('d', newPos, currentRoom) && !isDoor('d', newPos, currentRoom)) {
            throw new InvalidMoveException("That's a wall");
        } else if (isDoor('d', newPos, currentRoom)) {
            Room conRoom = currentRoom.getDoor("E").getOtherRoom(currentRoom);
            Door conDoor = conRoom.getDoor("W");
            thePlayer.setXyLocation(new Point(1, conDoor.getWallPosition()));
            thePlayer.setCurrentRoom(conRoom);
            currentRoom.setPlayerInRoom(false);
            conRoom.setPlayerInRoom(true);
            return ("Room " + conRoom.getId());
        }
        thePlayer.setXyLocation(newPos);
        return itemCheck(newPos, currentRoom);
    }

    protected String moveLeft() throws InvalidMoveException {
        Room currentRoom = thePlayer.getCurrentRoom();
        Point currentPos = thePlayer.getXyLocation();
        Point newPos = new Point(currentPos.x, currentPos.y);
        newPos.x--;
        if (isWall('a', newPos, currentRoom) && !isDoor('a', newPos, currentRoom)) {
            throw new InvalidMoveException("That's a wall");
        } else if (isDoor('a', newPos, currentRoom)) {
            Room conRoom = currentRoom.getDoor("W").getOtherRoom(currentRoom);
            Door conDoor = conRoom.getDoor("E");
            thePlayer.setXyLocation(new Point(conRoom.getWidth() - 2, conDoor.getWallPosition()));
            thePlayer.setCurrentRoom(conRoom);
            currentRoom.setPlayerInRoom(false);
            conRoom.setPlayerInRoom(true);
            return ("Room " + conRoom.getId());
        }
        thePlayer.setXyLocation(newPos);
        return itemCheck(newPos, currentRoom);
    }

    protected Boolean isWall(char dir, Point pos, Room currentRoom) {
        if (dir == 's') {
            if (pos.y >= currentRoom.getHeight() - 1) {
                return true;
            }
        } else if (dir == 'w') {
            if (pos.y <= 0) {
                return true;
            }
        } else if (dir == 'd') {
            if (pos.x >= currentRoom.getWidth() - 1) {
                return true;
            }
        } else if (dir == 'a') {
            if (pos.x <= 0) {
                return true;
            }
        }
        return false;
    }

    protected Boolean isDoor(char dir, Point pos, Room currentRoom) {
        Door toCheck;
        if (dir == 's') {
            return isDoorS(pos, currentRoom);
        } else if (dir == 'w') {
            return isDoorN(pos, currentRoom);
        } else if (dir == 'd') {
            return isDoorE(pos, currentRoom);
        } else if (dir == 'a') {
            return isDoorW(pos, currentRoom);
        }
        return false;
    }

    protected boolean isDoorS(Point pos, Room currentRoom) {
        Door toCheck = currentRoom.getDoor("S");
        if (toCheck != null) {
            if (pos.y == currentRoom.getHeight() - 1 && pos.x == toCheck.getWallPosition()) {
                return true;
            }
        }
        return false;
    }

    protected boolean isDoorN(Point pos, Room currentRoom) {
        Door toCheck = currentRoom.getDoor("N");
        if (toCheck != null) {
            if (pos.y == 0 && pos.x == toCheck.getWallPosition()) {
                return true;
            }
        }
        return false;
    }

    protected boolean isDoorE(Point pos, Room currentRoom) {
        Door toCheck = currentRoom.getDoor("E");
        if (toCheck != null) {
            if (pos.x == currentRoom.getWidth() - 1 && pos.y == toCheck.getWallPosition()) {
                return true;
            }
        }
        return false;
    }

    protected boolean isDoorW(Point pos, Room currentRoom) {
        Door toCheck = currentRoom.getDoor("W");
        if (toCheck != null) {
            if (pos.x == 0 && pos.y == toCheck.getWallPosition()) {
                return true;
            }
        }
        return false;
    }

    protected String itemCheck(Point pos, Room currentRoom) {
        ArrayList<Item> roomItems = currentRoom.getRoomItems();
        if (roomItems == null) {
            return " ";
        }
        for (Item i : roomItems) {
            Point itemPos = i.getXyLocation();
            if (itemPos.x == pos.x && itemPos.y == pos.y) {
                thePlayer.addItem(i);
                roomItems.remove(i);
                return i.getName();
            }
        }
        return " ";
    }

    /**
    * Returns a string telling the player they ate an item.
    * @param toEat the index of the item to eat
    * @return a string telling the player they ate an item
    */
    public String eatItem(int toEat) {
        String msg = "";
        if (toEat < 0 || toEat > thePlayer.getInventory().size() - 1) {
            return "Index out of bounds";
        } else if (thePlayer.getInventory().get(toEat) instanceof Food) {
            msg = ((Food) thePlayer.getInventory().get(toEat)).eat();
            thePlayer.getInventory().remove(toEat);
        } else if (thePlayer.getInventory().get(toEat) instanceof Potion) {
            msg = ((Potion) thePlayer.getInventory().get(toEat)).eat();
            thePlayer.getInventory().remove(toEat);
        } else {
            return "You can't eat that";
        }
        return msg;
    }

    /**
    * Returns a string telling the player they wore an item.
    * @param toWear the index of the item to wear
    * @return a string telling the player they wore an item
    */
    public String wearItem(int toWear) {
        String msg = "";
        if (toWear < 0 || toWear > thePlayer.getInventory().size() - 1) {
            return "Index out of bounds";
        } else if (thePlayer.getInventory().get(toWear) instanceof Clothing) {
            msg = ((Clothing) thePlayer.getInventory().get(toWear)).wear();
            thePlayer.equip(thePlayer.getInventory().get(toWear));
        } else if (thePlayer.getInventory().get(toWear) instanceof Ring) {
            msg = ((Ring) thePlayer.getInventory().get(toWear)).wear();
            thePlayer.equip(thePlayer.getInventory().get(toWear));
        } else {
            return "You can't wear that";
        }
        return msg;
    }

    /**
    * Returns a string telling the player they tossed an item.
    * @param toToss the index of the item to toss
    * @return a string telling the player they tossed an item
    */
    public String tossItem(int toToss) {
        String msg = "";
        if (toToss < 0 || toToss > thePlayer.getInventory().size() - 1) {
            return "Index out of bounds";
        } else if (thePlayer.getInventory().get(toToss) instanceof Potion) {
            msg = ((Potion) thePlayer.getInventory().get(toToss)).toss();
            dropItem(toToss);
        } else if (thePlayer.getInventory().get(toToss) instanceof SmallFood) {
            msg = ((SmallFood) thePlayer.getInventory().get(toToss)).toss();
            dropItem(toToss);
        } else {
            return "You can't toss that";
        }
        return msg;
    }

    protected void dropItem(int index) {
        Room currentRoom = thePlayer.getCurrentRoom();
        Item toDrop = thePlayer.getInventory().get(index);
        currentRoom.getRoomItems().add(toDrop);
        toDrop.setXyLocation(thePlayer.getXyLocation());
        thePlayer.getInventory().remove(index);
    }

    /**
    * Set the nextDisplay variable.
    * @param newDisplay the display string to be set
    */
    public void setNextDisplay(String newDisplay) {
        nextDisplay = newDisplay;
    }

    /**
    * Returns the next display calculated in makeMove.
    * @return a string representation of the next display
    */
    public String getNextDisplay() {
        return nextDisplay;
    }
}
