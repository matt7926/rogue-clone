package rogue;
import java.awt.Point;

import java.io.Serializable;

//A basic Item class; basic functionality for both consumables and equipment
public class Item implements Serializable {
    private static final long serialVersionUID = 3413910004278839405L;
    private int id;
    private String name;
    private String type;
    private Point xyLocation;
    private Character displayCharacter;
    private String description;
    private Room room;

    /**
    * Default constructor.
    */
    public Item() {
        setName(null);
        setType(null);
        setDescription(null);
    }

    /**
    * Constructor with parameters.
    * @param theId the item's id
    * @param theName the item's name
    * @param theType the item's type
    * @param theDescription the item's description
    */
    public Item(int theId, String theName, String theType, String theDescription) {
        setId(theId);
        setName(theName);
        setType(theType);
        setDescription(theDescription);
    }

    /**
    * Constructor with parameters.
    * @param theId the item's id
    * @param theName the item's name
    * @param theType the item's type
    * @param theDescription the item's description
    * @param theXyLocation the item's starting location
    */
    public Item(int theId, String theName, String theType, String theDescription, Point theXyLocation) {
        setId(theId);
        setName(theName);
        setType(theType);
        setDescription(theDescription);
        setXyLocation(theXyLocation);
    }

    /**
    * Returns the item's id.
    * @return the item's id
    */
    public int getId() {
        return id;
    }

    protected void setId(int newId) {
        id = newId;
    }

    /**
    * Returns the item's name.
    * @return the item's name
    */
    public String getName() {
        return name;
    }

    protected void setName(String newName) {
        name = newName;
    }

    /**
    * Returns the item's type.
    * @return the item's type
    */
    public String getType() {
        return type;
    }

    protected void setType(String newType) {
        type = newType;
    }

    /**
    * Returns the item's display character.
    * @return the item's display character
    */
    public Character getDisplayCharacter() {
        return displayCharacter;
    }

    protected void setDisplayCharacter(Character newDisplayCharacter) {
        displayCharacter = newDisplayCharacter;
    }

    /**
    * Returns the item's description.
    * @return the item's description
    */
    public String getDescription() {
        return description;
    }

    protected void setDescription(String newDescription) {
        description = newDescription;
    }

    /**
    * Returns the item's location.
    * @return the item's location
    */
    public Point getXyLocation() {
        return xyLocation;
    }

    protected void setXyLocation(Point newXyLocation) {
        xyLocation = newXyLocation;
    }

    /**
    * Returns the room the item is in.
    * @return the room the item is in
    */
    public Room getCurrentRoom() {
        return room;
    }

    protected void setCurrentRoom(Room newCurrentRoom) {
        room = newCurrentRoom;
    }
}
