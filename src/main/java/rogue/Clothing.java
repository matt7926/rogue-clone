package rogue;

public class Clothing extends Item implements Wearable {

    /**
    * Constructor with parameters.
    * @param theId the item's id
    * @param theName the item's name
    * @param theType the item's type
    * @param theDescription the item's description
    */
    public Clothing(int theId, String theName, String theType, String theDescription) {
        super(theId, theName, theType, theDescription);
    }

    /**
    * Returns a string telling the player they wore the item.
    * @return a string telling the player they wore the item
    */
    public String wear() {
        return "You put the " + this.getName() + " on: " + this.getDescription();
    }
}
