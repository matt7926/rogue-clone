package rogue;

public class Magic extends Item {
    /**
    * Constructor with parameters.
    * @param theId the item's id
    * @param theName the item's name
    * @param theType the item's type
    * @param theDescription the item's description
    */
    public Magic(int theId, String theName, String theType, String theDescription) {
        super(theId, theName, theType, theDescription);
    }
}
