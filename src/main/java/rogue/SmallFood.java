package rogue;

public class SmallFood extends Food implements Tossable {

    /**
    * Constructor with parameters.
    * @param theId the item's id
    * @param theName the item's name
    * @param theType the item's type
    * @param theDescription the item's description
    */
    public SmallFood(int theId, String theName, String theType, String theDescription) {
        super(theId, theName, theType, theDescription);
    }

    /**
    * Returns a string telling the player they threw the item.
    * @return a string telling the player they threw the item
    */
    public String toss() {
        return "You throw the " + getName() + " on the ground: " + getDescription().split(":")[1];
    }
}
