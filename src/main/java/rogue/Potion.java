package rogue;

public class Potion extends Magic implements Tossable, Edible {
    /**
    * Constructor with parameters.
    * @param theId the item's id
    * @param theName the item's name
    * @param theType the item's type
    * @param theDescription the item's description
    */
    public Potion(int theId, String theName, String theType, String theDescription) {
        super(theId, theName, theType, theDescription);
    }

    /**
    * Returns a string telling the player they drank the item.
    * @return a string telling the player they drank the item
    */
    public String eat() {
        return "You drink the " + getName() + ": " + getDescription().split(":")[0];
    }

    /**
    * Returns a string telling the player they threw the item.
    * @return a string telling the player they threw the item
    */
    public String toss() {
        return "You toss the " + getName() + " on the ground: " + getDescription().split(":")[1];
    }
}
