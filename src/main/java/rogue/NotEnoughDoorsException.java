package rogue;
public class NotEnoughDoorsException extends Exception {
    /**
    * Default constructor.
    */
    public NotEnoughDoorsException() {
        super();
    }

    /**
    * Constructor with single parameter.
    * @param message the error message
    */
    public NotEnoughDoorsException(String message) {
        super(message);
    }
}
