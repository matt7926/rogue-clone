package rogue;

//Subclass of item that is edible
public class Food extends Item implements Edible {

    /**
    * Constructor with parameters.
    * @param theId the item's id
    * @param theName the item's name
    * @param theType the item's type
    * @param theDescription the item's description
    */
    public Food(int theId, String theName, String theType, String theDescription) {
        super(theId, theName, theType, theDescription);
    }

    /**
    * Returns a string telling the player they ate the item.
    * @return a string telling the player they ate the item
    */
    public String eat() {
        return "You eat the " + this.getName() + ": " + this.getDescription().split(":")[0];
    }
}
