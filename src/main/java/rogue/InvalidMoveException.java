package rogue;
public class InvalidMoveException extends Exception {
    /**
    * Default constructor.
    */
    public InvalidMoveException() {
        super();
    }

    /**
    * Constructor with single parameter.
    * @param message the error message
    */
    public InvalidMoveException(String message) {
        super(message);
    }
}
